﻿using DesignStrategies.Interface;
using NUnit.Framework;
using DesignStrategies.SingeltonModule;

    [TestFixture]
    public class SingeltonTest
    {
        [Test]
        public void UseSingelton()
        {
            Singelton.SayHi();
           Singelton s1 = Singelton.Instance;
           Singelton s2 = Singelton.Instance;
           Assert.AreSame(s1,s2);

    }
       
    }
