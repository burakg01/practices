﻿using System;
using DesignStrategies.Interface;
using NUnit.Framework;

namespace UnitTest.Interface
{
    [TestFixture]
    public class InterfaceTest
    {
        [Test]
        public void ExpiredLicence()
        {
            Licence licence = new Licence(new DateTime(2000, 2, 2));
            Assert.IsTrue(licence.HasExpired);
        }
        [Test]
        public void NotExpiredLicence()
        {
            Licence licence = new Licence(new DateTime(2020, 2, 2));
            Assert.IsFalse(licence.HasExpired);
        }
    }
}