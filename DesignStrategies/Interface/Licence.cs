﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignStrategies.Interface
{
   public  class Licence
    {
        private readonly DateTime expiry;

        public Licence(DateTime expiry)
        {
            this.expiry = expiry;
        }
        public bool HasExpired
        {
            get { return DateTime.UtcNow > expiry; }
        }
    }
}
