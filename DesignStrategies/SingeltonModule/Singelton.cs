﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignStrategies.SingeltonModule
{
    public class Singelton
    {
        private static readonly Singelton instance = new Singelton();
        // forces laziness
        static Singelton()
        {
            // once only

        }

        private Singelton()
        {
            // Stuff that must be only happen once.
            Console.WriteLine("Singelton Constructor");
        }
        public static Singelton Instance
        {
            get { return instance; }
        }

        public static void SayHi()
        {
            Console.WriteLine("HIii");
        }

    }
}
